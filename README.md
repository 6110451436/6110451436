#ระบบ

#ผู้จัดทำ
   #นางสาว ศศินา  ขันธุวาร
   #รหัสนิสิต 6110451436

#รายละเอียดเบื้องต้น :
   #โครงงานนี้เป็นส่วนหนึ่งของรายวิชา 01418211 Software Construction ภาคปลาย 2562
   #“Jenis Fridge!” เป็น Application ที่สร้างขึ้นสำหรับผู้ใช้ที่ต้องการเก็บบันทึกอาหารในตู้เย็น สามารถตรวจสอบข้อมูลอาหารอาทิ ชื่อของอาหาร ประเภทของอาหาร หน่วยและปริมาณ ชนิดของอาหาร วันหมดอายุ นอกจากนี้ยังสามารถนำอาหารออกจากช่องแช่ได้ตามความต้องการของผู้ใช้
   #Jenis – ชื่อเล่นผู้จัดทำโครงงาน
   #Fridge – ตู้เย็น
   #Quote “live in the warm, eat good fresh foods” (ปรากฎในหน้าแรกของ Application)

#วันและเวลาการพัฒนาแอพพลิเคชั่น :
   #มอบหมายโครงงาน – 16/01/63
   #กำหนดส่ง – 25/03/63
#(Week 1) สร้าง GUI หน้าแรกของแอพพลิเคชั่นและตกแต่ง  / สร้าง ClassMainApp, Class FridgeControl
#(Week 2) สร้าง GUI หน้าโปรไฟล์และตกแต่ง / แก้ไข Class FridgeControl
#(Week 3) สร้าง GUI หน้าช่องแช่ของตู้เย็น / แก้ไขการตกแต่งของแอพพลิเคชั่น (เปลี่ยน theme)
#(Week 4) สร้าง GUI หน้าใส่ข้อมูลอาหาร / สร้าง Class Profile, Class FridgePage, Class FoodData
#(Week 5) สร้าง Class Food, Class FileFood / แก้ไขการตกแต่งของแอพพลิเคชั่น (เปลี่ยน theme)
#(Week 6) แก้ไข Class Food, Class FoodData / แก้ไขการตกแต่งของแอพพลิเคชั่น (เพิ่มปุ่ม remove)
#(Week 7) แก้ไข Class FridgePage, Class FoodData
#(Week 8) เพิ่มรูปภาพ / แก้ไข Class FridgePage, Class FoodData
#(Week 9) สร้าง GUI หน้า remove อาหาร / สร้าง Class RemoveFood, Class Button / เพิ่มและเปลี่ยนรูปภาพ
#(Week 10) ตรวจสอบความเรียบร้อยของโปรแกรมและแก้ไขข้อผิดพลาด

#การเก็บข้อมูล :
#เก็บ code ทั้งหมดไน folder src แยกเป็น 3 package
#เก็บภาพอาหารทั้งหมดใน folder - Data\food\imageFood








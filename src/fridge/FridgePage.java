package fridge;

import Food.FridgeFood;
import Food.FileFood;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class FridgePage {
    private FileFood fileFood;
    private ArrayList<FridgeFood> foods;
    private ArrayList<String> removeName;

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    private boolean isEmpty;

    @FXML
    Pane paneFreeze;

    @FXML
    ImageView imgFreeze1, imgFreeze2, imgNor1, imgNor2, imgNor3, imgNor4, imgNor5, imgNor6;

    @FXML
    Text freezeTxt1, freezeTxt2, normalTxt1, normalTxt2, normalTxt3, normalTxt4, normalTxt5, normalTxt6;

    @FXML
    Text freezeExp1, freezeExp2, normalExp1, normalExp2, normalExp3, normalExp4, normalExp5, normalExp6;

    public void initialize(){
        Platform.runLater(()-> {
            File food = new File("Data" + File.separator + "Food");
            fileFood = new FileFood(food);
            foods = fileFood.getFoods();

            ArrayList<FridgeFood> foodItem = fileFood.getFoods();

            for(FridgeFood f : foods) {
                LocalDate exp = LocalDate.parse(f.getExp());
                for(FridgeFood f1 : foodItem) {
                    if (exp.compareTo(LocalDate.parse(f1.getExp())) > 0) {
                        exp = LocalDate.parse(f1.getExp());
                    }
                }
            }

            showFood();
        });
    }

    public void showFood(){
        removeName = new ArrayList<>();
        int count = 0;
        for(FridgeFood food : foods) {
            if (removeName.indexOf(food.getName()) == -1) {
                removeName.add(food.getName());

                LocalDate exp = LocalDate.parse(food.getExp());
                int amount = 0;
                for (FridgeFood food1 : foods) {
                    if (food1.getName().equals(removeName.get(count))) {
                        amount++;
                        if (exp.compareTo(LocalDate.parse(food1.getExp())) > 0) {
                            exp = LocalDate.parse(food1.getExp());
                        }
                    }
                }
                count++;
                File image = new File(food.getUrl());
                if (food.isMelt()) {
                    if (freezeTxt1.getText().isEmpty()) {
                        imgFreeze1.setImage(new Image(image.toURI().toString()));
                        freezeTxt1.setText(food.getType() + ": " + food.getName() + " " + amount + " " + food.getUnit());
                        freezeExp1.setText(exp.toString());

                    } else if (freezeTxt2.getText().isEmpty()) {
                        imgFreeze2.setImage(new Image(image.toURI().toString()));
                        freezeTxt2.setText(food.getType() + ": " + food.getName() + " " + amount + " " + food.getUnit());
                        freezeExp2.setText(exp.toString());
                    }
                }
                if (!(food.isMelt())) {
                    if (normalTxt1.getText().isEmpty()) {
                        imgNor1.setImage(new Image(image.toURI().toString()));
                        normalTxt1.setText(food.getType() + ": " + food.getName() + " " + amount + " " + food.getUnit());
                        normalExp1.setText(exp.toString());
                    } else if (normalTxt2.getText().isEmpty()) {
                        imgNor2.setImage(new Image(image.toURI().toString()));
                        normalTxt2.setText(food.getType() + ": " + food.getName() + " " + amount + " " + food.getUnit());
                        normalExp2.setText(exp.toString());
                    } else if (normalTxt3.getText().isEmpty()) {
                        imgNor3.setImage(new Image(image.toURI().toString()));
                        normalTxt3.setText(food.getType() + ": " + food.getName() + " " + amount + " " + food.getUnit());
                        normalExp3.setText(exp.toString());
                    } else if (normalTxt4.getText().isEmpty()) {
                        imgNor4.setImage(new Image(image.toURI().toString()));
                        normalTxt4.setText(food.getType() + ": " + food.getName() + " " + amount + " " + food.getUnit());
                        normalExp4.setText(exp.toString());
                    } else if (normalTxt5.getText().isEmpty()) {
                        imgNor5.setImage(new Image(image.toURI().toString()));
                        normalTxt5.setText(food.getType() + ": " + food.getName() + " " + amount + " " + food.getUnit());
                        normalExp5.setText(exp.toString());
                    } else if (normalTxt6.getText().isEmpty()) {
                        imgNor6.setImage(new Image(image.toURI().toString()));
                        normalTxt6.setText(food.getType() + ": " + food.getName() + " " + amount + " " + food.getUnit());
                        normalExp6.setText(exp.toString());
                    }
                }
            }
        }
    }

    public void show(){
        removeName = new ArrayList<>();
        int foodCount = 0;

        for (int i = 0; i< foods.size(); i++) {

            if (removeName.indexOf(foods.get(i).getName()) == -1) {
                removeName.add(foods.get(i).getName());

                LocalDate exp = LocalDate.parse(foods.get(i).getExp());
                int amount = 0;
                for (int j=0; j<foods.size(); j++) {
                    if (foods.get(j).getName().equals(removeName.get(foodCount))) {
                        amount++;
                        if (exp.compareTo(LocalDate.parse(foods.get(j).getExp())) > 0) {
                            exp = LocalDate.parse(foods.get(j).getExp());
                        }
                    }
                }
                foodCount++;
            }
        }
    }



    @FXML
    public void menuBtn(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setTitle("Jenis Fridge!");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("welcomePage.fxml"));
        stage.setScene(new Scene(loader.load(),400, 500));
        FridgeControl controller = loader.getController();
        stage.show();
    }

    @FXML
    public void foodBtn(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setTitle("Food Data");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("foodData.fxml"));
        stage.setScene(new Scene(loader.load(),400, 500));
        FoodData foodData = loader.getController();
        stage.show();
    }

    @FXML
    public void removeBtn(ActionEvent event) throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setTitle("Remove Food");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("removeFood.fxml"));
        stage.setScene(new Scene(loader.load(),400,500));
        RemoveFood removeFood = loader.getController();

        if(!(freezeTxt1.getText().isEmpty())){
            String[] s = freezeTxt1.getText().split(" ");
            removeFood.setName(s[1].trim());
        }if(!(freezeTxt2.getText().isEmpty())){
            String[] s = freezeTxt2.getText().split(" ");
            removeFood.setName(s[1].trim());
        }if(!(normalTxt1.getText().isEmpty())){
            String[] s = normalTxt1.getText().split(" ");
            removeFood.setName(s[1].trim());
        }if(!(normalTxt2.getText().isEmpty())){
            String[] s = normalTxt2.getText().split(" ");
            removeFood.setName(s[1].trim());
        }if(!(normalTxt3.getText().isEmpty())){
            String[] s = normalTxt3.getText().split(" ");
            removeFood.setName(s[1].trim());
        }if(!(normalTxt4.getText().isEmpty())){
            String[] s = normalTxt4.getText().split(" ");
            removeFood.setName(s[1].trim());
        }if(!(normalTxt5.getText().isEmpty())){
            String[] s = normalTxt5.getText().split(" ");
            removeFood.setName(s[1].trim());
        }if(!(normalTxt6.getText().isEmpty())){
            String[] s = normalTxt6.getText().split(" ");
            removeFood.setName(s[1].trim());
        }
        stage.show();
    }
}

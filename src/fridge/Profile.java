package fridge;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class Profile {

    @FXML
    public void menuBtn(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setTitle("Jenis Fridge!");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("welcomePage.fxml"));
        stage.setScene(new Scene(loader.load(),400, 500));
        FridgeControl controller = loader.getController();
        stage.show();
    }
}

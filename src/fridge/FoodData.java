package fridge;

import Food.FileFood;
import Food.Food;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import Food.FridgeFood;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class FoodData{
    private ArrayList<FridgeFood> foods = new ArrayList<>();
    private FileFood fileFood;
    private FridgePage fridgePage;

    @FXML Pane pane;
    @FXML Text text;
    @FXML DatePicker exp;
    @FXML ComboBox<String> nameCombo, typeCombo, amountCombo;
    @FXML TextField unitField;

    public void initialize(){
        Platform.runLater(() -> {
            File file = new File("Data" + File.separator + "Food");
            fileFood = new FileFood(file);
            exp.setDayCellFactory(picker -> new DateCell() {
                public void updateItem(LocalDate date, boolean empty) {
                    super.updateItem(date, empty);
                    LocalDate today = LocalDate.now();
                    setDisable(empty || date.compareTo(today) < 0);
                }
            });
            typeCombo.getItems().addAll("delicatessen", "drink", "dessert", "fruit", "meat", "snack", "vegetable");
            amountCombo.getItems().addAll("1", "2", "3", "4", "5");
            typeCombo.setOnAction(event -> {
                nameCombo.getItems().clear();
                if(typeCombo.getValue().equals("drink")){
                    nameCombo.getItems().addAll("beer", "coffee", "coke", "ice", "juice", "milk", "soda", "tea", "water");
                    unitField.setText("bottle");
                }else if(typeCombo.getValue().equals("meat")){
                    nameCombo.getItems().addAll("beef", "chicken", "crab", "egg", "fish", "pork", "shell", "shrimp", "squid");
                    unitField.setText("kg.");
                }else if(typeCombo.getValue().equals("snack")){
                    nameCombo.getItems().addAll("biscuit", "candy", "chocolate", "cookie", "jelly", "potato chip");
                    unitField.setText("piece");
                }else if(typeCombo.getValue().equals("dessert")){
                    nameCombo.getItems().addAll("bread", "cake", "donut", "ice-cream", "pancake", "pie", "pudding", "toast", "waffle", "whipping cream");
                    unitField.setText("piece");
                }else if(typeCombo.getValue().equals("vegetable")){
                    nameCombo.getItems().addAll("broccoli", "cabbage", "carrot", "cauliflower", "chili", "cucumber", "garlic", "ginger", "lemon", "mushroom", "onion", "parsley", "pea", "potato", "scallion", "tomato");
                    unitField.setText("kg.");
                }else if(typeCombo.getValue().equals("fruit")){
                    nameCombo.getItems().addAll("apple", "avocado", "banana", "cantaloupe", "cherry", "coconut", "grape", "kiwi", "mango", "orange", "pineapple", "pomegranate", "strawberry", "watermelon");
                    unitField.setText("kg.");
                }else if(typeCombo.getValue().equals("delicatessen")){
                    nameCombo.getItems().addAll("butter", "cheese", "pickled-fruit", "pickled-vegetable", "spaghetti", "yogurt");
                    unitField.setText("piece");
                }
            });
        });
    }


    public void addBtn(Event event){
        fridgePage = new FridgePage();
        String name = nameCombo.getSelectionModel().getSelectedItem();
        String type = typeCombo.getSelectionModel().getSelectedItem();
        String amount = amountCombo.getSelectionModel().getSelectedItem();
        text.setVisible(false);
        if(name == null){
            text.setVisible(true);
        }else if(type == null){
            text.setVisible(true);
        }else if(amount == null){
            text.setVisible(true);
        } else if(exp.getValue() == null){
            text.setVisible(true);
        } else{
            for (int i = 0; i < Integer.parseInt(amountCombo.getValue()); i++) {
                FridgeFood food = new FridgeFood(type, name, "1", unitField.getText(), exp.getValue());
                fileFood.addFood(food);
            }
        }
    }


    @FXML
    public void fridgeBtn(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setTitle("Fridge View");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("fridgePage.fxml"));
        stage.setScene(new Scene(loader.load(),400, 500));
        FridgePage fridgePage = loader.getController();
        stage.show();
    }
}

package fridge;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class FridgeControl {
    @FXML
    public void profileBtn(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setTitle("Profile");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("profile.fxml"));
        stage.setScene(new Scene(loader.load(),400, 500));
        Profile profile = loader.getController();
        stage.show();
    }
    @FXML
    public void fridgeBtn(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setTitle("Fridge View");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("fridgePage.fxml"));
        stage.setScene(new Scene(loader.load(),400, 500));
        FridgePage fridgePage = loader.getController();
        stage.show();
    }
}

package fridge;

import Food.FridgeFood;
import Food.FileFood;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class RemoveFood {
    private ArrayList<FridgeFood> foods;
    private FileFood fileFood;
    private FridgePage fridgePage;
    private ArrayList<String> show;
    @FXML
    ComboBox<String> nameRemove;
    @FXML
    Text error, txtName;
    @FXML
    ImageView imgExp;


    public void initialize(){
        File food = new File("Data" + File.separator + "Food");
        fileFood = new FileFood(food);
        foods = fileFood.getFoods();
        error.setVisible(false);

    }

    public void setName(String nameRemove) {
        this.nameRemove.getItems().add(nameRemove);
    }

    @FXML
    public void removeFoodBtn(Event event){
        if(nameRemove.getValue() == null) {
            error.setVisible(true);
            return;
        }
        for(FridgeFood food : foods){
            System.out.println(food.getName());
            if(food.getName().equals(nameRemove.getValue())) {
                File image = new File(food.getUrl());
                imgExp.setImage(new Image(image.toURI().toString()));
                txtName.setText(nameRemove.getValue() + "\n" + food.getExp());
            }
        }
        error.setVisible(false);
        fileFood.reduceFood(fileFood.findExp(nameRemove.getValue()));
        fileFood.add();
    }

    @FXML
    public void fridgeBtn(ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setTitle("Fridge View");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("fridgePage.fxml"));
        stage.setScene(new Scene(loader.load(),400, 500));
        FridgePage fridgePage = loader.getController();
        stage.show();
    }

}

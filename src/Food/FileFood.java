package Food;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FileFood {
    private ArrayList<FridgeFood> foods;
    private File file;

    public FileFood(File fileFood) {
        this.file = fileFood;
        File foodFile = new File(fileFood.getPath() + File.separator + "RefrigeratorData.csv");
        System.out.println();
        if(!foodFile.exists()){
            try{
                foodFile.createNewFile();
            }catch (IOException e){
                System.err.println("Can't created file");
            }
        }
        foods = new ArrayList<>();
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(foodFile));
            String s;
            while ((s = buffer.readLine()) != null){
                System.out.println(s);
                String[] info = s.split(",");
                String type = info[0].trim();
                String name = info[1].trim();
                String amount = info[2].trim();
                String unit = info[3].trim();
                LocalDate exp = LocalDate.parse(info[4].trim());
                FridgeFood food = new FridgeFood(type, name, amount, unit, exp);
                foods.add(food);
            }
        }catch (FileNotFoundException e){
            System.err.println("Not found.");
        }catch (IOException e){
            System.err.println("Read error.");
        }
    }

    public void addFood(FridgeFood food) {
        foods.add(food);
        add();
    }

    public ArrayList<FridgeFood> showName(String name) {
        ArrayList<FridgeFood> showName = new ArrayList<>();
        for (FridgeFood food : foods) {
            if (food.getName().equals(name)) {
                showName.add(food);
            }
        } return showName;
    }

    public void add(){
        File foodList = new File(file.getPath() + File.separator + "RefrigeratorData.csv");
        BufferedWriter buffer = null;
        try{
            buffer = new BufferedWriter(new FileWriter(foodList));
            for(Food foodFile : foods){
                buffer.write(foodFile.toString());
                buffer.newLine();
            }
            buffer.flush();
            buffer.close();
        }catch (FileNotFoundException e){
            System.err.println("Not found.");
        }catch (IOException e){
            System.err.println("Can't saved.");
        }
    }

    public FridgeFood findExp(String name){
        ArrayList<FridgeFood> foods = showName(name);
        FridgeFood time = foods.get(0);
        LocalDate exp = LocalDate.parse(foods.get(0).getExp());
        for (FridgeFood food : foods) {
            if (exp.compareTo(LocalDate.parse(food.getExp())) > 0) {
                exp = LocalDate.parse(food.getExp());
                time = food;
            }
        }
        return time;
    }

    public int reduceFood(FridgeFood name) {
        ArrayList<FridgeFood> reduceFood = new ArrayList<>();
        for (FridgeFood food : foods){
            if (food.getName().equals(name.getName()) && food.getExp().equals(name.getExp())) {
                reduceFood.add(food);
            }
        }
        foods.removeAll(reduceFood);
        return foods.size();
    }



    public ArrayList<FridgeFood> getFoods() {
        return foods;
    }
}

package Food;

import java.io.File;

public class Food{
    private String type;
    private String name;
    private int amount;
    private String unit;
    private String url;


    public Food(String type, String name, String amount, String unit) {
        this.type = type;
        this.name = name;
        this.amount = Integer.parseInt(amount);
        this.unit = unit;
        this.url = "Data" + File.separator + "Food" + File.separator + "imageFood" + File.separator + name + ".png";
    }

    public void addAmount(String amount){ this.amount += Integer.parseInt(amount); }

    public void Remove(int amount){ this.amount -= amount; }

    public String getName() {
        return name;
    }

    public String getType() { return type; }

    public String getAmount() { return amount + ""; }

    public String getUnit() { return unit; }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() { return type + "," +name + "," + amount + "," + unit; }

}

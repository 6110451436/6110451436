package Food;

import java.time.LocalDate;

public class FridgeFood extends Food implements Freezable{
    private LocalDate exp;
    private boolean isMelt;

    public FridgeFood(String type, String name, String amount, String unit, LocalDate exp) {
        super(type, name, amount, unit);
        this.exp = exp;
        isMelt = false;
        if(name.equals("ice-cream") || name.equals("ice")||name.equals("fish")|| name.equals("crab") || name.equals("shell")||name.equals("shrimp")||name.equals("squid")||name.equals("chocolate")||name.equals("whipping cream")||name.equals("spaghetti")){
            setMelt(true);
        }
    }

    public String getExp() { return exp.toString(); }

    public void setExp(LocalDate exp) { this.exp = exp; }

    public void setMelt(boolean melt) { isMelt = melt; }

    @Override
    public boolean isMelt() {
        return isMelt;
    }

    @Override
    public String toString() {
        return getType() + "," + getName() + "," + getAmount() + "," + getUnit() + "," + exp;
    }
}
